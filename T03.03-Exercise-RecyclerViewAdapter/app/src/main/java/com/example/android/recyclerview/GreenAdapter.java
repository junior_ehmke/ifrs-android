/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.example.android.recyclerview;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

// TODO (4) Modifique GreenAdapter para estender RecyclerView.Adapter<NumberViewHolder>
public class GreenAdapter {

    // TODO (1) Adcione uma variável privada int chamada mNumberItems

    // TODO (2) Crie um construtor para GreenAdapter que aceita um int como parâmetro chamado numberOfItems
    // TODO (3) Armazene numberOfItems em mNumberItems

    // TODO (5) Sobrescreva o método onCreateViewHolder
    // TODO (6) Dentro do método, crie e retorne um novo NumberViewHolder

    // TODO (7) Sobrescreva o método onBindViewHolder
    // TODO (8) Dentro do método, chame holder.bind e passe a posição

    // TODO (9) Sobrescreva o método getItemCount e retorne o número de itens a ser exibido

    /**
     * Cache of the children views for a list item.
     */
    class NumberViewHolder extends RecyclerView.ViewHolder {

        // Will display the position in the list, ie 0 through getItemCount() - 1
        TextView listItemNumberView;

        /**
         * Constructor for our ViewHolder. Within this constructor, we get a reference to our
         * TextViews and set an onClickListener to listen for clicks. Those will be handled in the
         * onClick method below.
         * @param itemView The View that you inflated in
         *                 {@link GreenAdapter#onCreateViewHolder(ViewGroup, int)}
         */
        public NumberViewHolder(View itemView) {
            super(itemView);

            listItemNumberView = (TextView) itemView.findViewById(R.id.tv_item_number);
        }

        /**
         * A method we wrote for convenience. This method will take an integer as input and
         * use that integer to display the appropriate text within a list item.
         * @param listIndex Position of the item in the list
         */
        void bind(int listIndex) {
            listItemNumberView.setText(String.valueOf(listIndex));
        }
    }
}
